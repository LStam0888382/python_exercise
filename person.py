class Person(object):
    def __init__(self, name, person_type):
        self.name = name
        self.person_type = person_type

    def get_name(self):
        return self.name

    def get_type(self):
        return self.person_type
