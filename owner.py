from person import Person


class Owner(Person):
    def __init__(self, name):
        super().__init__(name, "owner")
        self.vehicles = list()

    def add_vehicle(self, vehicle):
        self.vehicles.append(vehicle)

    def remove_vehicle(self, vehicle):
        self.vehicles.remove(vehicle)
