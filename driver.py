from person import Person


class Driver(Person):
    def __init__(self, name):
        super().__init__(name, "driver")
