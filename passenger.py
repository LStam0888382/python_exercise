from person import Person


class Passenger(Person):
    def __init__(self, name):
        super().__init__(name, "passenger")
