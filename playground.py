from car import Car
from bike import Bike
from canoe import Canoe
from owner import Owner
from driver import Driver
from passenger import Passenger
import time

if __name__ == '__main__':
    print("start")

    owner_l = Owner("Leon")
    owner_t = Owner("Tomas")

    driver_l = Driver("Leon")
    driver_t = Driver("Tomas")

    passenger_1 = Passenger("Leon")
    passenger_2 = Passenger("Henk")

    car_1 = Car("Seat Leon", owner_t)
    car_2 = Car("Seat Ibiza", owner_l)
    car_3 = Car("Mazda mazda 3", owner_l)

    bike_1 = Bike("Koga Miyata touring bike", owner_t)
    bike_2 = Bike("Giant mountain bike", owner_t)
    bike_3 = Bike("Batavus city bike", owner_t)
    bike_4 = Bike("Rusty bike", owner_l)

    canoe_1 = Canoe("Dagger canoe", owner_t)

    car_1.set_driver(owner_t)
    print(car_1.start())
    print(car_1.accelerate())
    time.sleep(5)
    print(car_1.accelerate())
    time.sleep(5)
    print(car_1.get_distance_driven())
    print(car_1.stop())

    print(car_1.get_speed())
    print(car_1.get_time_running())
    print(car_1.get_distance_driven())
    print(car_1.start())
    print(car_1.get_time_running())
