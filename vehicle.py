import time
from engine import Engine


class Vehicle(object):
    def __init__(self, name, vehicle_type, max_amount_of_passengers, owner, start_sound="????", amount_of_wheels=0):
        self.name = name
        self.amount_of_wheels = amount_of_wheels
        self.speed = 0
        self.start_sound = start_sound
        self.last_speed_change_time = None
        self.driven_distance = 0
        self.vehicle_type = vehicle_type
        self.engine = None
        self.start_time = time.localtime()
        self.owner = owner
        self.driver = None
        self.passengers = list()
        self.max_amount_of_passengers = max_amount_of_passengers
        if self.vehicle_type == "car":
            self.engine = Engine(self.start_sound)

    def __time_running(self):
        return time.mktime(time.localtime()) - time.mktime(self.start_time)

    def __accumulate_distance(self):
        if self.last_speed_change_time is not None:
            time_const_speed = time.mktime(time.localtime()) - time.mktime(self.last_speed_change_time)
            self.driven_distance = self.driven_distance + time_const_speed * (self.speed / 3.6)

        self.last_speed_change_time = time.localtime()

    def __possible_to_change_speed(self):
        if self.engine is not None:
            if self.engine.is_motor_started:
                return True
            else:
                return False
        else:
            return True

    def accelerate(self):
        if self.__possible_to_change_speed():
            self.__accumulate_distance()
            self.speed = self.speed + 10
            if self.speed > 200:
                self.speed = 200
            return "CurrentSpeed = {} km/h".format(self.speed)
        else:
            error = "{} is not started.".format(self.name)
            raise RuntimeError(error)

    def decelerate(self):
        if self.__possible_to_change_speed():
            self.__accumulate_distance()
            self.speed = self.speed - 10
            if self.speed < -50:
                self.speed = -50
            return "CurrentSpeed = {} km/h".format(self.speed)
        else:
            error = "{} is not started.".format(self.name)
            raise RuntimeError(error)

    def get_speed(self):
        return "CurrentSpeed = {} km/h".format(self.speed)

    def start(self):
        if self.driver is not None:
            if self.driver.name == self.owner.name:
                try:
                    self.last_speed_change_time = time.localtime()
                    return self.engine.start()
                except AttributeError as e:
                    error = "{} has no engine.".format(self.vehicle_type)
                    raise RuntimeError(error)
            else:
                error = "The owner is {}, only he can start the {}.".format(self.owner.name, self.vehicle_type)
                raise RuntimeError(error)
        else:
            error = "Someone has to drive the {} before starting.".format(self.vehicle_type)
            raise RuntimeError(error)

    def stop(self):
        try:
            self.__accumulate_distance()
            speed = 0
            self.engine.stop()
            return "Stopped!"
        except AttributeError as e:
            error = "{} has no engine.".format(self.vehicle_type)
            raise RuntimeError(error)

    def get_time_running(self):
        return "Time driving: {}  seconds".format(self.__time_running())

    def get_distance_driven(self):
        if self.last_speed_change_time is not None:
            time_const_speed = time.mktime(time.localtime()) - time.mktime(self.last_speed_change_time)
            distance = self.driven_distance + time_const_speed * (self.speed / 3.6)
        else:
            distance = 0

        ret = "Distance: {} m in {} seconds".format(distance, self.__time_running())

        return ret

    def set_driver(self, driver):
        if self.engine is not None:
            if self.engine.is_motor_started:
                error = "Before changing the driver stop the {}".format(self.vehicle_type)
                raise RuntimeError(error)

        self.driver = driver

    def add_passenger(self, passenger):
        if len(self.passengers) < self.max_amount_of_passengers:
            self.passengers.append(passenger)
        else:
            error = "{} is full".format(self.name)
            raise RuntimeError(error)

    def remove_passenger(self, passenger):
        try:
            self.passengers.remove(passenger)
        except ValueError:
            raise RuntimeError("Passenger not in the {}.".format(self.vehicle_type))
