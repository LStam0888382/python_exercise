from vehicle import Vehicle


class Canoe(Vehicle):
    def __init__(self, name, owner):
        super().__init__(name, "canoe", 4, owner, "vrrrroem", 4)
