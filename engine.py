import time


class Engine(object):
    def __init__(self, start_sound):
        self.is_motor_started = False
        self.start_time = None
        self.start_sound = start_sound

    def start(self):
        self.is_motor_started = True
        self.start_time = time.localtime()
        return self.start_sound

    def stop(self):
        self.start_time = None
        self.is_motor_started = False

    def is_motor_started(self):
        return self.is_motor_started

    def get_time_running(self):
        if not self.is_motor_started:
            raise RuntimeError("Get time running before starting.")

        return time.mktime(time.localtime()) - time.mktime(self.start_time)

    def get_start_time(self):
        return time.asctime(self.start_time())