from vehicle import Vehicle


class Bike(Vehicle):
    def __init__(self, name, owner):
        super().__init__(name, "bike", 1, owner, "vrrrroem", 4)

    def wheelie(self):
        if self.speed >= 10 and self.speed <= 20:
            return "Wheelie!!!!"
        elif self.speed > 20:
            return "You are to fast!"
        else:
            return "To slow!!"
